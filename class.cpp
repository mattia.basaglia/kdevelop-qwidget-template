{% extends "cpp_qobject_implementation.cpp" %}


{% block includes %}
{{ block.super }}
#include "ui_{{ output_file_ui|cut:".ui" }}.h"
#include <QEvent>


class {{name}}::Private
{
public:
    Ui::{{name}} ui;
};
{% endblock includes %}

{% block function_definitions %}
{{ name }}::{{ name }}(QWidget* parent)
{% block public_constructor_init_list %}
    : {% for base in base_classes %}{{ base.baseType }}{% endfor %}(parent), d(std::make_unique<Private>())
{% endblock public_constructor_init_list %}
{
{% block public_constructor_body %}
{{ block.super }}
    d->ui.setupUi(this);
{% endblock public_constructor_body %}
}



{{ name }}::~{{ name }}() = default;



void {{ name }}::changeEvent ( QEvent* e )
{
    {% for base in base_classes %}{{ base.baseType }}{% endfor %}::changeEvent(e);



    if ( e->type() == QEvent::LanguageChange)
    {
        d->ui.retranslateUi(this);
    }
}
{% endblock function_definitions %}
