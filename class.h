{% extends "cpp_qobject_header.h" %}


{% block qobject_default_include %}
#include <QWidget>
{% endblock qobject_default_include %}


{% block includes %}
#include <memory>

{{ block.super }}
{% endblock includes %}


{% block class_declaration_open %}
{% if base_classes %}
{% include "class_declaration_cpp.txt" %}
{
{% else %}
class {{ name }} : public {% block qobject_baseclass %}QWidget{% endblock qobject_baseclass %}
{
{% endif %}
{% endblock class_declaration_open %}

{% block class_body %}

    Q_OBJECT



public:
    {{ name }}(QWidget* parent = nullptr);
    ~{{ name }}();



protected:
    void changeEvent ( QEvent* e ) override;



private:
    class Private;
    std::unique_ptr<Private> d;
{% endblock class_body %}
